# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::CoordinatedPipeline::Notifier do
  let(:deploy_version) { '14.3.202108261121-1c87faa073d.2cec58b7eb5' }
  let(:environment) { 'gstg' }
  let(:fake_client) { stub_const('ReleaseTools::GitlabOpsClient', spy) }
  let(:slack_channel) { nil }

  let(:fake_notification) do
    stub_const('ReleaseTools::Slack::CoordinatedPipelineNotification', spy)
  end

  subject(:notifier) do
    described_class.new(
      pipeline_id: 123,
      deploy_version: '14.3.202108261121-1c87faa073d.2cec58b7eb5',
      environment: environment,
      slack_channel: slack_channel
    )
  end

  describe '#execute' do
    context 'when downstream pipeline can be found' do
      let(:gstg_deployer_pipeline) do
        create(:pipeline, :running, web_url: 'https://test.gitlab.net/deployer/-/pipelines/123')
      end

      let(:cny_deployer_pipeline) do
        create(:pipeline, :running, web_url: 'https://test.gitlab.net/deployer/-/pipelines/456')
      end

      let(:gprd_deployer_pipeline) do
        create(:pipeline, :running, web_url: 'https://test.gitlab.net/deployer/-/pipelines/789')
      end

      let(:gstg_bridge) do
        create(
          :gitlab_response,
          name: 'deploy:gstg',
          downstream_pipeline: gstg_deployer_pipeline
        )
      end

      let(:cny_bridge) do
        create(
          :gitlab_response,
          name: 'deploy:gprd-cny',
          downstream_pipeline: cny_deployer_pipeline
        )
      end

      let(:gprd_bridge) do
        create(
          :gitlab_response,
          name: 'deploy:gprd',
          downstream_pipeline: gprd_deployer_pipeline
        )
      end

      before do
        allow(fake_client)
          .to receive(:pipeline_bridges)
          .and_return([gstg_bridge, cny_bridge, gprd_bridge])

        allow(fake_notification)
          .to receive(:new)
          .and_return(double(execute: nil))
      end

      it 'sends a slack notification' do
        expect(ReleaseTools::GitlabOpsClient).to receive(:pipeline_bridges).once
        expect(ReleaseTools::Slack::CoordinatedPipelineNotification)
          .to receive(:new)
          .with(
            deploy_version: deploy_version,
            pipeline: gstg_deployer_pipeline,
            environment: 'gstg'
          )

        without_dry_run do
          notifier.execute
        end
      end

      context 'with slack_channel' do
        let(:slack_channel) { 'channel' }

        it 'sends a slack notification to given channel' do
          expect(ReleaseTools::Slack::CoordinatedPipelineNotification)
            .to receive(:new)
            .with(
              deploy_version: deploy_version,
              pipeline: gstg_deployer_pipeline,
              environment: 'gstg',
              slack_channel: slack_channel
            )

          without_dry_run do
            notifier.execute
          end
        end
      end

      context 'with a different environment' do
        let(:environment) { 'gprd-cny' }

        it 'sends a slack notification' do
          expect(ReleaseTools::Slack::CoordinatedPipelineNotification)
            .to receive(:new)
            .with(
              deploy_version: deploy_version,
              pipeline: cny_deployer_pipeline,
              environment: 'gprd-cny'
            )

          without_dry_run do
            notifier.execute
          end
        end
      end
    end

    context "when downstream pipeline can't be found" do
      before do
        allow(fake_client)
          .to receive(:pipeline_bridges)
          .and_return([])

        allow(fake_notification)
          .to receive(:new)
          .and_return(double(execute: nil))
      end

      it 'sends a slack notification' do
        expect(ReleaseTools::Slack::CoordinatedPipelineNotification)
          .to receive(:new)
          .with(
            deploy_version: deploy_version,
            pipeline: nil,
            environment: 'gstg'
          )

        without_dry_run do
          notifier.execute
        end
      end

      it 'retries finding the pipeline' do
        expect(ReleaseTools::GitlabOpsClient).to receive(:pipeline_bridges).at_least(:twice)

        without_dry_run do
          notifier.execute
        end
      end

      context 'when bridge does not have downstream pipeline' do
        let(:gstg_bridge) do
          create(
            :gitlab_response,
            name: 'deploy:gstg',
            downstream_pipeline: nil
          )
        end

        before do
          allow(fake_client)
            .to receive(:pipeline_bridges)
            .and_return([gstg_bridge])
        end

        it 'retries finding the pipeline' do
          expect(ReleaseTools::GitlabOpsClient).to receive(:pipeline_bridges).at_least(:twice)
          expect(gstg_bridge).to receive(:downstream_pipeline).at_least(:once)

          without_dry_run do
            notifier.execute
          end
        end
      end
    end
  end
end
