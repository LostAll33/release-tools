# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::CoordinatedPipeline::Qa::Notifier do
  let(:fake_client) { stub_const('ReleaseTools::GitlabOpsClient', spy) }

  let(:fake_notification) do
    stub_const('ReleaseTools::Slack::QaNotification', spy)
  end

  subject(:notifier) do
    described_class.new(
      pipeline_id: '123',
      deploy_version: '14.6.202112031820-3605bffdaf6.c516dd235a1',
      environment: 'gstg'
    )
  end

  describe '#execute' do
    context 'when qa pipeline can be found' do
      let(:gstg_deployer_pipeline) do
        create(
          :pipeline,
          :success,
          web_url: 'https://test.gitlab.net/deployer/-/pipelines/123'
        )
      end

      let(:qa_smoke_pipeline) do
        create(
          :pipeline,
          :failed,
          web_url: 'https://test.gitlab.net/quality/-/pipelines/456'
        )
      end

      let(:gstg_bridge) do
        create(
          :gitlab_response,
          name: 'deploy:gstg',
          downstream_pipeline: gstg_deployer_pipeline
        )
      end

      let(:qa_smoke_bridge) do
        create(
          :gitlab_response,
          name: 'qa:smoke:gstg',
          status: 'failed',
          downstream_pipeline: qa_smoke_pipeline
        )
      end

      it 'sends a slack notification' do
        allow(fake_client)
          .to receive(:pipeline_bridges)
          .and_return([gstg_bridge, qa_smoke_bridge])

        allow(fake_notification)
          .to receive(:new)
          .and_return(double(execute: nil))

        expect(ReleaseTools::GitlabOpsClient)
          .to receive(:pipeline_bridges)
          .once

        expect(ReleaseTools::Slack::QaNotification)
          .to receive(:new)
          .with(
            deploy_version: '14.6.202112031820-3605bffdaf6.c516dd235a1',
            pipeline: qa_smoke_pipeline,
            environment: 'gstg'
          )

        notifier.execute
      end
    end

    context "when qa pipeline can't be found" do
      it 'does nothing' do
        allow(fake_client)
          .to receive(:pipeline_bridges)
          .and_return([])

        expect(ReleaseTools::Slack::CoordinatedPipelineNotification)
          .not_to receive(:new)

        notifier.execute
      end
    end
  end
end
