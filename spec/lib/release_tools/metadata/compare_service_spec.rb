# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metadata::CompareService do
  let(:instance) { described_class.new(source: source, environment: 'gstg') }

  describe '#with_latest_successful_deployment' do
    subject(:compare_latest) { instance.with_latest_successful_deployment }

    let(:source) { ReleaseTools::ProductVersion.new('14.6.202112160620') }
    let(:target_version) { ReleaseTools::ProductVersion.new('14.6.202112151320') }
    let(:deployments) { [double(sha: 'previous_sha')] }

    before do
      project = ReleaseTools::Project::Release::Metadata

      allow(source).to receive(:metadata_commit_id).and_return('source_sha')

      allow(ReleaseTools::ProductVersion)
        .to receive(:from_metadata_sha)
        .with('previous_sha')
        .and_return(target_version)

      allow(ReleaseTools::GitlabOpsClient)
        .to receive(:deployments)
        .with(project, 'gstg', order_by: 'id', sort: 'desc', status: 'success', opts: { per_page: 1 })
        .and_return(deployments)
    end

    it 'returns the correct previous deployment version' do
      expect(compare_latest).to be_instance_of(ReleaseTools::Metadata::Comparison)
      expect(compare_latest.source).to eq(source)
      expect(compare_latest.target).to eq(target_version)
    end

    context 'when target version is older than source version' do
      let(:source) { ReleaseTools::ProductVersion.new('14.6.202112151320') }
      let(:target_version) { ReleaseTools::ProductVersion.new('14.6.202112160620') }

      it 'inverts source and target' do
        expect(compare_latest).to be_instance_of(ReleaseTools::Metadata::Comparison)
        expect(compare_latest.source).to eq(target_version)
        expect(compare_latest.target).to eq(source)
      end
    end
  end
end
