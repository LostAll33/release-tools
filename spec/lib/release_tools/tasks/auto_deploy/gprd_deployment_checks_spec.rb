# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::AutoDeploy::GprdDeploymentChecks do
  describe '#execute' do
    let(:production_status) do
      instance_spy(
        ReleaseTools::Promotion::ProductionStatus,
        fine?: false,
        failed_checks: [ReleaseTools::Promotion::Checks::ActiveIncidents],
        to_slack_blocks: production_status_slack_blocks
      )
    end

    let(:slack_message) do
      [{
        type: "section",
        text: {
          type: "mrkdwn",
          text: ":red_circle: <!subteam^S0127FU8PDE> GPRD deployment blocked\n\n" \
                "Package version: `14.8.202202070320-25de243b9f7.8a87d2a97c6`\n" \
                "Deployment: <http://example.com|pipeline>\n"
        }
      }]
    end

    let(:production_status_slack_blocks) { ['production status'] }
    let(:pipeline_url) { 'http://example.com' }
    let(:deploy_version) { '14.8.202202070320-25de243b9f7.8a87d2a97c6' }

    subject(:execute) { described_class.new.execute }

    around do |ex|
      ClimateControl.modify(CI_PIPELINE_URL: pipeline_url, DEPLOY_VERSION: deploy_version, &ex)
    end

    before do
      allow(ReleaseTools::Promotion::ProductionStatus).to receive(:new).and_return(production_status)
    end

    it 'raises error' do
      expect(ReleaseTools::Slack::ChatopsNotification)
        .to receive(:fire_hook)
        .with(
          blocks: slack_message + production_status_slack_blocks,
          text: "GPRD deployment blocked",
          channel: ReleaseTools::Slack::F_UPCOMING_RELEASE
        )

      expect { execute }.to raise_error(described_class::UnsafeProductionError, 'production checks have failed')
    end

    context 'when production checks pass' do
      let(:production_status) { instance_spy(ReleaseTools::Promotion::ProductionStatus, fine?: true) }

      it 'does nothing' do
        expect(ReleaseTools::Slack::ChatopsNotification).not_to receive(:fire_hook)

        expect { execute }.not_to raise_error
      end
    end
  end
end
