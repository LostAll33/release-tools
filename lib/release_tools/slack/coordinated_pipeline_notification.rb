# frozen_string_literal: true

require 'release_tools/time_util'

module ReleaseTools
  module Slack
    class CoordinatedPipelineNotification
      include ::SemanticLogger::Loggable
      include ReleaseTools::TimeUtil
      include Utilities

      # deploy_version  - Deployer package
      # pipeline        - GitLab response pipeline object
      # environment     - gstg-ref, gstg-cny, gstg, gprd-cny, gprd
      def initialize(deploy_version:, pipeline:, environment:)
        @deploy_version = deploy_version
        @pipeline = pipeline
        @environment = environment
      end

      def execute
        logger.info('Sending slack notification', deploy_version: deploy_version, environment: environment, deployer_url: deployer_url)

        response = ReleaseTools::Slack::Message.post(
          channel: ReleaseTools::Slack::ANNOUNCEMENTS,
          message: fallback_text,
          blocks: slack_block
        )

        return response unless send_diffs_notification?

        send_threaded_diffs_notification(response['ts'])
      end

      private

      attr_reader :deploy_version, :pipeline, :environment

      def deployer_url
        pipeline&.web_url
      end

      def fallback_text
        "#{environment} #{deployer_status} #{deploy_version}"
      end

      def slack_block
        [
          {
            type: 'section',
            text: ReleaseTools::Slack::Webhook.mrkdwn(section_block)
          },
          {
            type: 'context',
            elements: context_elements
          }
        ]
      end

      def section_block
        [].tap do |text|
          text << environment_icon
          text << status_icon
          text << "*#{environment}*"
          text << deployer_status_link
          text << "`#{deploy_version}`"
        end.join(' ')
      end

      def deployer_status_link
        if pipeline
          "<#{deployer_url}|#{deployer_status}>"
        else
          "Downstream pipeline couldn't be found, check if `#{bridge_job_name}` job in <#{ENV['CI_PIPELINE_URL']}|coordinated pipeline> has started."
        end
      end

      def bridge_job_name
        "deploy:#{environment}"
      end

      def context_elements
        [].tap do |elements|
          elements << clock_context_element
          elements << { type: 'mrkdwn', text: ":sentry: #{sentry_link}" }
          elements << { type: 'mrkdwn', text: ":timer_clock: #{wall_duration}" } if finished_or_failed?
        end
      end

      def sentry_link
        version = ReleaseTools::AutoDeploy::Version.new(deploy_version)

        "<https://sentry.gitlab.net/gitlab/gitlabcom/releases/#{version.rails_sha}/|View Sentry>"
      end

      def wall_duration
        duration(current_time - start_time).first
      end

      def start_time
        Time.parse(pipeline.created_at)
      end

      def finished_or_failed?
        deployer_status == 'finished' || deployer_status == 'failed'
      end

      def send_diffs_notification?
        deployer_status == 'started'
      end

      def send_threaded_diffs_notification(thread_ts)
        args = {
          deploy_version: deploy_version,
          environment: environment,
          thread_ts: thread_ts
        }

        ReleaseTools::Slack::CoordinatedPipelineDiffsNotification.new(args).execute
      end
    end
  end
end
